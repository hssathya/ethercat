################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Src/aoeappl.c \
../src/Src/coeappl.c \
../src/Src/ecataoe.c \
../src/Src/ecatappl.c \
../src/Src/ecatcoe.c \
../src/Src/ecateoe.c \
../src/Src/ecatfoe.c \
../src/Src/ecatslv.c \
../src/Src/ecatsoe.c \
../src/Src/eoeappl.c \
../src/Src/foeappl.c \
../src/Src/mailbox.c \
../src/Src/mcihw.c \
../src/Src/objdef.c \
../src/Src/sdoserv.c 

OBJS += \
./src/Src/aoeappl.o \
./src/Src/coeappl.o \
./src/Src/ecataoe.o \
./src/Src/ecatappl.o \
./src/Src/ecatcoe.o \
./src/Src/ecateoe.o \
./src/Src/ecatfoe.o \
./src/Src/ecatslv.o \
./src/Src/ecatsoe.o \
./src/Src/eoeappl.o \
./src/Src/foeappl.o \
./src/Src/mailbox.o \
./src/Src/mcihw.o \
./src/Src/objdef.o \
./src/Src/sdoserv.o 

C_DEPS += \
./src/Src/aoeappl.d \
./src/Src/coeappl.d \
./src/Src/ecataoe.d \
./src/Src/ecatappl.d \
./src/Src/ecatcoe.d \
./src/Src/ecateoe.d \
./src/Src/ecatfoe.d \
./src/Src/ecatslv.d \
./src/Src/ecatsoe.d \
./src/Src/eoeappl.d \
./src/Src/foeappl.d \
./src/Src/mailbox.d \
./src/Src/mcihw.d \
./src/Src/objdef.d \
./src/Src/sdoserv.d 


# Each subdirectory must supply rules for building sources it contributes
src/Src/%.o: ../src/Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -I"D:\Ethercat\project\ZC702_AXI_VIVADO_R\ZC702_AXI_VIVADO.sdk\test\src\ethercat_ssc" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -I../../test_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


