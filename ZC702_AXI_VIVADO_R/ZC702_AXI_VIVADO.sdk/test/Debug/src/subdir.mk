################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/gpio_handler.c \
../src/helloworld.c \
../src/pips_handler.c \
../src/platform.c \
../src/timer_handler.c 

OBJS += \
./src/gpio_handler.o \
./src/helloworld.o \
./src/pips_handler.o \
./src/platform.o \
./src/timer_handler.o 

C_DEPS += \
./src/gpio_handler.d \
./src/helloworld.d \
./src/pips_handler.d \
./src/platform.d \
./src/timer_handler.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -I"D:\Ethercat\project\GIT\ZC702_AXI_VIVADO_R\ZC702_AXI_VIVADO.sdk\test\src\ethercat_ssc" -I"D:\Ethercat\project\GIT\ZC702_AXI_VIVADO_R\ZC702_AXI_VIVADO.sdk\test\src" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -I../../test_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


