################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ethercat_ssc/coeappl.c \
../src/ethercat_ssc/ecatappl.c \
../src/ethercat_ssc/ecatcoe.c \
../src/ethercat_ssc/ecateoe.c \
../src/ethercat_ssc/ecatfoe.c \
../src/ethercat_ssc/ecatslv.c \
../src/ethercat_ssc/eoeappl.c \
../src/ethercat_ssc/foeappl.c \
../src/ethercat_ssc/mailbox.c \
../src/ethercat_ssc/mcihw.c \
../src/ethercat_ssc/objdef.c \
../src/ethercat_ssc/sdoserv.c \
../src/ethercat_ssc/testappl.c 

OBJS += \
./src/ethercat_ssc/coeappl.o \
./src/ethercat_ssc/ecatappl.o \
./src/ethercat_ssc/ecatcoe.o \
./src/ethercat_ssc/ecateoe.o \
./src/ethercat_ssc/ecatfoe.o \
./src/ethercat_ssc/ecatslv.o \
./src/ethercat_ssc/eoeappl.o \
./src/ethercat_ssc/foeappl.o \
./src/ethercat_ssc/mailbox.o \
./src/ethercat_ssc/mcihw.o \
./src/ethercat_ssc/objdef.o \
./src/ethercat_ssc/sdoserv.o \
./src/ethercat_ssc/testappl.o 

C_DEPS += \
./src/ethercat_ssc/coeappl.d \
./src/ethercat_ssc/ecatappl.d \
./src/ethercat_ssc/ecatcoe.d \
./src/ethercat_ssc/ecateoe.d \
./src/ethercat_ssc/ecatfoe.d \
./src/ethercat_ssc/ecatslv.d \
./src/ethercat_ssc/eoeappl.d \
./src/ethercat_ssc/foeappl.d \
./src/ethercat_ssc/mailbox.d \
./src/ethercat_ssc/mcihw.d \
./src/ethercat_ssc/objdef.d \
./src/ethercat_ssc/sdoserv.d \
./src/ethercat_ssc/testappl.d 


# Each subdirectory must supply rules for building sources it contributes
src/ethercat_ssc/%.o: ../src/ethercat_ssc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -I"D:\Ethercat\project\GIT\ZC702_AXI_VIVADO_R\ZC702_AXI_VIVADO.sdk\test\src\ethercat_ssc" -I"D:\Ethercat\project\GIT\ZC702_AXI_VIVADO_R\ZC702_AXI_VIVADO.sdk\test\src" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -I../../test_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


