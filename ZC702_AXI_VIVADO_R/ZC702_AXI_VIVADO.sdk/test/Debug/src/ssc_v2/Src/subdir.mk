################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ssc_v2/Src/aoeappl.c \
../src/ssc_v2/Src/coeappl.c \
../src/ssc_v2/Src/ecataoe.c \
../src/ssc_v2/Src/ecatappl.c \
../src/ssc_v2/Src/ecatcoe.c \
../src/ssc_v2/Src/ecateoe.c \
../src/ssc_v2/Src/ecatfoe.c \
../src/ssc_v2/Src/ecatslv.c \
../src/ssc_v2/Src/ecatsoe.c \
../src/ssc_v2/Src/eoeappl.c \
../src/ssc_v2/Src/foeappl.c \
../src/ssc_v2/Src/mailbox.c \
../src/ssc_v2/Src/mcihw.c \
../src/ssc_v2/Src/objdef.c \
../src/ssc_v2/Src/sdoserv.c 

OBJS += \
./src/ssc_v2/Src/aoeappl.o \
./src/ssc_v2/Src/coeappl.o \
./src/ssc_v2/Src/ecataoe.o \
./src/ssc_v2/Src/ecatappl.o \
./src/ssc_v2/Src/ecatcoe.o \
./src/ssc_v2/Src/ecateoe.o \
./src/ssc_v2/Src/ecatfoe.o \
./src/ssc_v2/Src/ecatslv.o \
./src/ssc_v2/Src/ecatsoe.o \
./src/ssc_v2/Src/eoeappl.o \
./src/ssc_v2/Src/foeappl.o \
./src/ssc_v2/Src/mailbox.o \
./src/ssc_v2/Src/mcihw.o \
./src/ssc_v2/Src/objdef.o \
./src/ssc_v2/Src/sdoserv.o 

C_DEPS += \
./src/ssc_v2/Src/aoeappl.d \
./src/ssc_v2/Src/coeappl.d \
./src/ssc_v2/Src/ecataoe.d \
./src/ssc_v2/Src/ecatappl.d \
./src/ssc_v2/Src/ecatcoe.d \
./src/ssc_v2/Src/ecateoe.d \
./src/ssc_v2/Src/ecatfoe.d \
./src/ssc_v2/Src/ecatslv.d \
./src/ssc_v2/Src/ecatsoe.d \
./src/ssc_v2/Src/eoeappl.d \
./src/ssc_v2/Src/foeappl.d \
./src/ssc_v2/Src/mailbox.d \
./src/ssc_v2/Src/mcihw.d \
./src/ssc_v2/Src/objdef.d \
./src/ssc_v2/Src/sdoserv.d 


# Each subdirectory must supply rules for building sources it contributes
src/ssc_v2/Src/%.o: ../src/ssc_v2/Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -I"D:\Ethercat\project\ZC702_AXI_VIVADO_R\ZC702_AXI_VIVADO.sdk\test\src\ethercat_ssc" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -I../../test_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


