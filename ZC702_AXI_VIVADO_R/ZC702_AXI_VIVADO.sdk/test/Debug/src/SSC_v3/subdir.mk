################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/SSC_v3/coeappl.c \
../src/SSC_v3/ecatappl.c \
../src/SSC_v3/ecatcoe.c \
../src/SSC_v3/ecateoe.c \
../src/SSC_v3/ecatfoe.c \
../src/SSC_v3/ecatslv.c \
../src/SSC_v3/eoeappl.c \
../src/SSC_v3/foeappl.c \
../src/SSC_v3/mailbox.c \
../src/SSC_v3/mcihw.c \
../src/SSC_v3/objdef.c \
../src/SSC_v3/sdoserv.c \
../src/SSC_v3/testappl.c 

OBJS += \
./src/SSC_v3/coeappl.o \
./src/SSC_v3/ecatappl.o \
./src/SSC_v3/ecatcoe.o \
./src/SSC_v3/ecateoe.o \
./src/SSC_v3/ecatfoe.o \
./src/SSC_v3/ecatslv.o \
./src/SSC_v3/eoeappl.o \
./src/SSC_v3/foeappl.o \
./src/SSC_v3/mailbox.o \
./src/SSC_v3/mcihw.o \
./src/SSC_v3/objdef.o \
./src/SSC_v3/sdoserv.o \
./src/SSC_v3/testappl.o 

C_DEPS += \
./src/SSC_v3/coeappl.d \
./src/SSC_v3/ecatappl.d \
./src/SSC_v3/ecatcoe.d \
./src/SSC_v3/ecateoe.d \
./src/SSC_v3/ecatfoe.d \
./src/SSC_v3/ecatslv.d \
./src/SSC_v3/eoeappl.d \
./src/SSC_v3/foeappl.d \
./src/SSC_v3/mailbox.d \
./src/SSC_v3/mcihw.d \
./src/SSC_v3/objdef.d \
./src/SSC_v3/sdoserv.d \
./src/SSC_v3/testappl.d 


# Each subdirectory must supply rules for building sources it contributes
src/SSC_v3/%.o: ../src/SSC_v3/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -I"D:\Ethercat\project\ZC702_AXI_VIVADO_R\ZC702_AXI_VIVADO.sdk\test\src\ethercat_ssc" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -I../../test_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


