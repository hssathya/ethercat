/*
* This source file is part of the EtherCAT Slave Stack Code licensed by Beckhoff Automation GmbH & Co KG, 33415 Verl, Germany.
* The corresponding license agreement applies. This hint shall not be removed.
*/

/**
\addtogroup TestAppl Tests Application
@{
 */
 
/**
\file Testappl.c
\author EthercatSSC@beckhoff.com
\brief Implementation
This application can be used to generate specific (also non conform) slave behaviour to test the correct master reaction
See the Application Note ET9300 for further information about the test and the application in general

\version 5.12

<br>Changes to version V5.11:<br>
V5.12 ECAT1: update SM Parameter measurement (based on the system time), enhancement for input only devices and no mailbox support, use only 16Bit pointer in process data length caluclation<br>
V5.12 ECAT2: big endian changes<br>
V5.12 EOE2: prevent static ethernet buffer to be freed<br>
V5.12 EOE3: fix memory leaks in sample ICMP application<br>
V5.12 TEST1: test pending ESM handling (APPL_StartMailboxHandler shall only called once), try to overwrite the error in case of ErrSafeOP<br>
V5.12 TEST2: add pending ESM test,trigger complete ESM transition from ecat main<br>
<br>Changes to version V5.10:<br>
V5.11 DIAG4: change parameter handling in DIAG_CreateNewMessage()<br>
V5.11 ECAT11: create application interface function pointer, add eeprom emulation interface functions<br>
V5.11 ECAT4: enhance SM/Sync monitoring for input/output only slaves<br>
V5.11 TEST2: add test 0x300C object (entry bitlength > 65535)<br>
V5.11 TEST8: create diag message on SDO write access to 0x3002<br>
V5.11 TEST9: "add behaviour 0x2020.7 (SDO requests on 0x3006.0 are set to pending until an FoE read request on ""UnlockSdoResp"" is received or in case that no mbx queue is supported when a new mbx request was received)"<br>
<br>Changes to version V5.01:<br>
V5.10 DIAG1: Define diagmessage textIDs<br>
V5.10 ECAT6: Add "USE_DEFAULT_MAIN" to enable or disable the main function<br>
V5.10 TEST5: test 0x2020.1 change limit from 10 to 16 Byte <br>
             Add test object 0x3009/0x300A (huge array and record objects)<br>
V5.10 TEST6: Add Bit3Array (0x3007) test object<br>
V5.10 TEST7: Add Test behavior control via User RAM (see SSC Application Note for further details)<br>
V5.10 TEST8: Add ESM variables Object 0xA000 and map to process data (0x1AFF)<br>
<br>Changes to version V5.0:<br>
V5.01 SDO6: Update SDO response interface handling. (used if the object access function returns "ABORTIDX_WORKING" and SDO_RES_INTERFACE is active)<br>
V5.01 TEST1: Length of ESC access rights array were wrong<br>
V5.01 TEST5: Add slave configuration object handling (0x8000, if test application is active)<br>
V5.0: file created
 */


/*-----------------------------------------------------------------------------------------
------
------    Includes
------
-----------------------------------------------------------------------------------------*/
#ifdef _WIN32
#include <windows.h>
#endif

#include "ecat_def.h"
#include "gpio_handler.h"



#define _TESTAPPL_
#include "testappl.h"

//#undef _TESTAPPL_



#include "ecatfoe.h"


/*--------------------------------------------------------------------------------------
------
------    local Types and Defines
------
--------------------------------------------------------------------------------------*/
//#define TEST_ESC_PDI_ACCESS_RIGHTS      //if a set of ESC registers will be checked if the access rights are correct (this feature is currently only for 8Bit_ESC_ACCESS available)

#undef DebugMessage



#ifdef TEST_ESC_PDI_ACCESS_RIGHTS

typedef struct {
    UINT16 EscRegister;
    UINT8 Flags;
} RegisterWriteFlag;

//Writeable registers from PDI (based on ET1100)
RegisterWriteFlag EscPdiWriteAccess[] =
{
    {0x0012, 0xFF},
    {0x0013, 0xFF},
    {0x0130, 0xFF},
    {0x0131, 0xFF},
    {0x0134, 0xFF},
    {0x0135, 0xFF},
    {0x0204, 0xFF},
    {0x0205, 0xFF},
    {0x0206, 0xFF},
    {0x0207, 0xFF},
    /*EEPROM Registers skipped (generally read only)*/
    /*MII Registers skipped (generally read only)*/
    {0x080F, 0x03}, //SM0 PDI Control
    {0x0817, 0x03}, //SM1 PDI Control
    {0x081F, 0x03}, //SM2 PDI Control
    {0x0827, 0x03}, //SM3 PDI Control
    {0x082F, 0x03}, //SM4 PDI Control
    {0x0837, 0x03}, //SM5 PDI Control
    {0x083F, 0x03}, //SM6 PDI Control
    {0x0847, 0x03}, //SM7 PDI Control
    {0x084F, 0x03} //SM8 PDI Control
    /*DC Register skipped (0x900 - 0x9FF)*/
    /*GPIO skipped (0xF10 - 0xF17)*/
    /*UserRAM skipped (0xF80 - 0xFFF)*/
};

#define WR_MASK_LENGTH (SIZEOF(EscPdiWriteAccess) / SIZEOF(RegisterWriteFlag))
#endif


#define MAX_FILE_SIZE                             0x180


UINT32 u32FileSize;

UINT16 u16Busycnt;
UINT32           nFileWriteOffset;
UINT16 MBXMEM     aFoeData[(MAX_FILE_SIZE >> 1)];

/*-----------------------------------------------------------------------------------------
------
------    local variables and constants
------
-----------------------------------------------------------------------------------------*/
UINT8 SDOResult = 0; /**< \brief used to access object via SDO response stack (every first object access will be set to pending)*/

UINT32 CurrentActiveTest = 0; /**< \brief Contains the current active test. This value is used if the tests are activated via the user RAM area of the ESC (0xF80:0xF83), see SSC application note for further details.*/


extern TSDOINFOENTRYDESC OBJMEM asEntryDesc0x3009[];

/*Create broadcast Ethernet address*/
const    UINT8    BroadcastEthernetAddress[6] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

/*-----------------------------------------------------------------------------------------
------
------    local (test) functions
------
-----------------------------------------------------------------------------------------*/
void HandleTestControlViaUserRAM(void);




/////////////////////////////////////////////////////////////////////////////////////////
/**

\brief    This function calculates a checksum (only for an even number of bytes).
\brief Note that if you are going to checksum a checksummed packet that includes the checksum,
\brief you have to compliment the output.

*////////////////////////////////////////////////////////////////////////////////////////

UINT16 EOEAPPL_CalcCheckSum(UINT16 MBXMEM *pWord, UINT16 nLen)
{
    UINT32 crc;
    UINT32 CrcLo;
    UINT32 CrcHi;
    UINT16 RetCrc;

        crc = 0;
    while (nLen > 1)
    {
            crc += SWAPWORD(*pWord);
        pWord++;
        nLen -= 2;
    }
    if (nLen == 1)                          // if nLen odd
    {
            crc += *((UINT8*)pWord);
    }

    CrcLo = LOWORD(crc);
    CrcHi = HIWORD(crc);
    crc = CrcLo + CrcHi;

    CrcHi = HIWORD(crc);
    crc += CrcHi;
    if (crc == 0xFFFF)                     // remove the -0 ambiguity
    {
            crc = 0;
    }

    RetCrc = (UINT16)crc;
    RetCrc = ~RetCrc;
    return(RetCrc);
}

void EoeReceive(UINT16 *pFrame, UINT16 frameSize)
{
/*ECATCHANGE_START(V5.12) ECAT2*/
    switch (SWAPWORD(((ETHERNET_FRAME *)pFrame)->FrameType))
/*ECATCHANGE_END(V5.12) ECAT2*/
    {
    case ETHERNET_FRAME_TYPE_ARP1_SW:
    {
        ETHERNET_FRAME MBXMEM * pSendFrame = (ETHERNET_FRAME MBXMEM *) ALLOCMEM(frameSize);
        ARP_IP_HEADER MBXMEM    * pArpIp = (ARP_IP_HEADER MBXMEM    *) &pSendFrame[1];

        /*Copy Receive Frame to create ARP Reply*/
        MBXMEMCPY(pSendFrame, pFrame, frameSize);
        if ((MBXMEMCMP(BroadcastEthernetAddress, pSendFrame->Destination.w, 4) == 0)
            && (pArpIp->lengthHwAddrPortAddr == 0x0406) /*lowbyte: length of MAC address; highbyte length of port address*/
            && (pArpIp->hwAddrSpace == SWAPWORD(ARP_HW_ADDR_SPACE_ETHERNET_SW))
            && (pArpIp->protAddrSpace == SWAPWORD(ETHERNET_FRAME_TYPE_IP_SW))
            && (pArpIp->opcode == SWAPWORD(ARP_OPCODE_REQUEST_SW))
            )
        {
            MBXMEMCPY(pSendFrame->Destination.w, pSendFrame->Source.w, 6);
            MBXMEMCPY(pSendFrame->Source.w, &aMacAdd[0], 6);

            MBXMEMCPY(pArpIp->macDest.w, pArpIp->macSource.w, 6);
            MBXMEMCPY(pArpIp->macSource.w, &aMacAdd[0], 6);
            MBXMEMCPY(pArpIp->ipDest, pArpIp->ipSource, 4);
            MBXMEMCPY(pArpIp->ipSource, aIpAdd, 4);

            pArpIp->opcode = SWAPWORD(ARP_OPCODE_REPLY_SW);

            EOE_SendFrameRequest((UINT16 MBXMEM *) pSendFrame, ARP_IP_HEADER_LEN + ETHERNET_FRAME_LEN);
        }
/*ECATCHANGE_START(V5.12) EOE3*/
        else
        {
            if (pSendFrame != NULL)
            {
                FREEMEM(pSendFrame);
                pSendFrame = NULL;
            }
        }
/*ECATCHANGE_END(V5.12) EOE3*/
    }
        break;
    case ETHERNET_FRAME_TYPE_IP_SW:
    {
        ETHERNET_IP_ICMP_MAX_FRAME MBXMEM * pIPHeader = (ETHERNET_IP_ICMP_MAX_FRAME MBXMEM *) ALLOCMEM(frameSize);

        /*Copy Receive Frame to create ICMP Reply*/
        MBXMEMCPY(pIPHeader, pFrame, frameSize);

        
        if ((((SWAPWORD(pIPHeader->Ip.ttlProtocol) & IP_HEADER_PROTOCOL_MASK)>> 8) == IP_PROTOCOL_ICMP)
            && ((SWAPWORD(pIPHeader->IpData.Icmp.typeCode) & ICMP_TYPE_MASK) == ICMP_TYPE_ECHO)
            && (MBXMEMCMP(pIPHeader->Ip.dest, aIpAdd, 4) == 0)
            )
        {
            // ping requested
            UINT16 length;
            UINT16 lo = 0;
            UINT16 hi = 0;
            UINT32 tmp;

            // length is in BigEndian format -> swap bytes
            lo = ((pIPHeader->Ip.length) & 0xff) << 8;
            hi = pIPHeader->Ip.length >> 8;
            length = hi + lo;
            // swap src and dest ip address
            MEMCPY(&tmp, pIPHeader->Ip.src, 4);
            MEMCPY(pIPHeader->Ip.src, pIPHeader->Ip.dest, 4);
            MEMCPY(pIPHeader->Ip.dest, &tmp, 4);

            // set ping reply command
            pIPHeader->IpData.Icmp.typeCode &= ~ICMP_TYPE_MASK;
            pIPHeader->IpData.Icmp.typeCode |= ICMP_TYPE_ECHO_REPLY;

            // swap src and dest mac address
            MBXMEMCPY(pIPHeader->Ether.Destination.w, pIPHeader->Ether.Source.w, 6);
            MBXMEMCPY(pIPHeader->Ether.Source.w, aMacAdd, 6);


            // calculate ip checksum
            pIPHeader->Ip.cksum = 0;
            pIPHeader->Ip.cksum = SWAPWORD(EOEAPPL_CalcCheckSum((UINT16 MBXMEM *) &pIPHeader->Ip, IP_HEADER_MINIMUM_LEN));
            // calculate icmp checksum
            pIPHeader->IpData.Icmp.checksum = 0;
            /* type cast because of warning was added */
            pIPHeader->IpData.Icmp.checksum = SWAPWORD(EOEAPPL_CalcCheckSum((UINT16 MBXMEM *) &pIPHeader->IpData.Icmp, (UINT16)(length - 20)));
            /* type cast because of warning was added */
            EOE_SendFrameRequest((UINT16 MBXMEM *) pIPHeader, (UINT16)(ETHERNET_FRAME_LEN + length));
        }
        else
        {
            //protocol not supported => free allocated buffer
            if (pIPHeader != NULL)
            {
                FREEMEM(pIPHeader);
                pIPHeader = NULL;
            }
        }
    }
        break;
    }
}


UINT16 FoEWriteData(UINT16 MBXMEM * pData, UINT16 Size, BOOL bDataFollowing)
{
    if (Size)
    {
        if((nFileWriteOffset + Size)> MAX_FILE_SIZE)
        {
            return 0x8003; //ECAT_FOE_ERRCODE_DISKFULL (0x8003)
        }
        else
        {

        MBXMEMCPY(&aFoeData[(nFileWriteOffset >> 1)], pData, Size);
        }
    }

    if (bDataFollowing)
    {
        /* FoE-Data services will follow */
        nFileWriteOffset += Size;

    }
    else
    {
        /* last part of the file is written */
        u32FileSize = nFileWriteOffset + Size;
        nFileWriteOffset = 0;
    }

    return 0;
}

UINT16 FoEWrite(UINT16 MBXMEM * pName, UINT16 nameSize, UINT32 password)
{
    if (password != 0x12345678)
    {
        return ECAT_FOE_ERRCODE_NORIGHTS;
    }

    u32FileSize = 0;
    nFileWriteOffset = 0;
    return 0;
}

UINT16 FoERead(UINT16 MBXMEM * pName, UINT16 nameSize, UINT32 password, UINT16 maxBlockSize, UINT16 *pData)
{
    UINT16 i = 0;
    CHAR aReadFileName[20];
    UINT16 sizeError = 0;

    if (password != 0x12345678)
    {
        return ECAT_FOE_ERRCODE_NORIGHTS;
    }

    if ((nameSize + 1) > 20)
    {
        return  ECAT_FOE_ERRCODE_DISKFULL;
    }


    if (u32FileSize == 0)
    {
        /* No file stored*/
        return ECAT_FOE_ERRCODE_NOTFOUND;
    }

    sizeError = maxBlockSize;

    if (u32FileSize < sizeError)
    {
        sizeError = (UINT16)u32FileSize;
    }

    /*Read requested file name to endianess conversion if required*/
    MBXTOOBJSTRCPY(aReadFileName, pName, nameSize);

    aReadFileName[nameSize] = '\0';


    if (IS_TEST_ACTIVE(DelaySdoResponse0x3006))
    {
        bUnlockSdoRequest = TRUE;
        CHAR aUnlock[] = "UnlockSdoResp";

        for (i = 0; i < nameSize; i++)
        {
            if (aUnlock[i] != aReadFileName[i])
            {
                bUnlockSdoRequest = FALSE;
                break;
            }

        }

        if (bUnlockSdoRequest)
        {
            sizeError = 10;
        }

    }


    if (IS_TEST_ACTIVE(FoE0_ReadBusy))
    {
        INC_TEST_CNT(FoE0_ReadBusy);


        if (pName != NULL)
        {
            sizeError = (FOE_MAXBUSY - 50); /* Return 50% busy*/
        }
        else
        {
            sizeError = 0;
        }
    }

    if (sizeError <= FOE_MAXDATA)
    {
        /*copy the first foe data block*/
        MEMCPY(pData, aFoeData, sizeError);
    }


    return sizeError;
}

UINT16 FoEReadData(UINT32 offset, UINT16 maxBlockSize, UINT16 *pData)
{
    UINT16 sizeError = 0;

    if (u32FileSize < offset)
    {
        return 0;
    }

    /*get file length to send*/
    sizeError = (UINT16)(u32FileSize - offset);


    if (sizeError > maxBlockSize)
    {
        /*transmit max block size if the file data to be send is greater than the max data block*/
        sizeError = maxBlockSize;
    }

    //to support also 16bit only platfroms an byte FoE fragments are not supported
    if ((offset % 2) > 0)
    {
        return 0x8004; //ECAT_FOE_ERRCODE_ILLEGAL (0x8004)
    }

    /*copy the foe data block 2 .. n*/
    MEMCPY(pData, &(((UINT16 *)aFoeData)[(offset >> 1)]), sizeError);

    return sizeError;
}




#ifdef TEST_ESC_PDI_ACCESS_RIGHTS
/////////////////////////////////////////////////////////////////////////////////////////
/**
  \brief     test write access of ESC registers.
             - read register value 
             - toggle bits and write register
 *////////////////////////////////////////////////////////////////////////////////////////

void ESC_Access_Test() 
{
    UINT16 RegOffset = 0;
    UINT16 MaskArrCnt = 0;
    UINT8 RdData = 0;
    UINT8 NewData = 0;
    UINT8 RdMask = 0xFF;

    for (RegOffset = 0; RegOffset < 0x1000; RegOffset++) 
    {
        /*Reset ReadFlagsMask;*/
        RdMask = 0xFF;

        if (MaskArrCnt < WR_MASK_LENGTH)
        {
            if (EscPdiWriteAccess[MaskArrCnt].EscRegister == RegOffset) 
            {
                RdMask = ~EscPdiWriteAccess[MaskArrCnt].Flags;
                MaskArrCnt++;
            }
        }

        HW_EscReadByte(RdData, RegOffset);
        NewData = ~RdData;

        HW_EscWriteByte(NewData, RegOffset);
        HW_EscReadByte(NewData, RegOffset);

#ifdef DebugMessage
        if ((RdData & RdMask) != (NewData & RdMask)) 
        {
            DebugMessage("Invalid Register Access (reg: 0x%x)", RegOffset);
        }
#endif
    }
}
#endif

void APPL_Main()
{
    if (i8EoeSendPending == 1)
    {
        UINT16 result = EOE_SendFrameRequest(pObj3017_EoeSendData, Obj3017_EoeSendDataLength);
        
        if(result == 0)
            i8EoeSendPending = 0;
    }
}
/////////////////////////////////////////////////////////////////////////////////////////
/**
 \return    result of the application initialization and basic Tests

 \brief     Initialize application variables and perform some basics tests
 *////////////////////////////////////////////////////////////////////////////////////////

UINT16 APPL_Init(void) 
{
    UINT16 result = 0;

    UINT32 InitValue = 0;
    UINT32 cnt = 0;

    HW_EscWriteDWord(InitValue, ESC_TESTID_REGISTER);
    UpdateActiveTest();

    for (cnt = 0; cnt < HUGE_OBJ_BYTE_SIZE; cnt++) 
    {
        HugeObj[cnt] = (UINT8) (cnt & 0xFF);
    }

    /*reset test control object*/
    sTestControlObj.u16SubIndex0 = TEST_CONTROL_BITS;
    for (cnt = 0; cnt < TEST_CONTROL_BITS; cnt++) 
    {
        sTestControlObj.aEntries[cnt].TestIndex = 0;
        sTestControlObj.aEntries[cnt].TestSubindex = 0;
        sTestControlObj.aEntries[cnt].pTest = NULL;
    }

#ifdef TEST_ESC_PDI_ACCESS_RIGHTS
    ESC_Access_Test();
#endif

    /*Reset last index indication*/
    CurrentActiveTest = 0x00000000;
    HW_EscWriteDWord(CurrentActiveTest, 0xF84);

    /* Reset Test id value */
    CurrentActiveTest = 0xFFFFFFFF;
    HW_EscWriteDWord(CurrentActiveTest, 0xF80);

    bUnlockSdoRequest = FALSE;


    pAPPL_EoeReceive = EoeReceive;

    pAPPL_FoeRead = FoERead;
    pAPPL_FoeReadData = FoEReadData;
    pAPPL_FoeWrite = FoEWrite;
    pAPPL_FoeWriteData = FoEWriteData;


    /*ECATCHANGE_START(V5.12) TEST2*/
    bPendingESMLock = FALSE;
    /*ECATCHANGE_END(V5.12) TEST2*/

    i8EoeSendPending = -1;
    pAPPL_MainLoop = APPL_Main;

    return result;
}

/*-----------------------------------------------------------------------------------------
------
------    generic functions
------
-----------------------------------------------------------------------------------------*/
/////////////////////////////////////////////////////////////////////////////////////////
/**
 \brief    The function is called when an error state was acknowledged by the master

 *////////////////////////////////////////////////////////////////////////////////////////

void APPL_AckErrorInd(UINT16 stateTrans) 
{
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \return    AL Status Code (see ecatslv.h ALSTATUSCODE_....)

 \brief    The function is called in the state transition from INIT to PREOP when
           all general settings were checked to start the mailbox handler. This function
           informs the application about the state transition, the application can refuse
           the state transition when returning an AL Status error code.
           The return code NOERROR_INWORK can be used, if the application cannot confirm
           the state transition immediately, in that case the application need to be complete 
           the transition by calling ECAT_StateChange.

 *////////////////////////////////////////////////////////////////////////////////////////

UINT16 APPL_StartMailboxHandler(void) 
{



/*ECATCHANGE_START(V5.12) TEST2*/
    if (IS_TEST_ACTIVE(ESM0_PENDING_TRANSITIONS))
    {
        if (bPendingESMLock)
        {
            //function is called the second time
            return ALSTATUSCODE_NOVALIDFIRMWARE;
        }
        else
        {
            INC_TEST_CNT(ESM0_PENDING_TRANSITIONS);
            bPendingESMLock = TRUE;
            return NOERROR_INWORK;
        }
    }
/*ECATCHANGE_END(V5.12) TEST2*/


    return ALSTATUSCODE_NOERROR;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \return     0, NOERROR_INWORK

 \brief    The function is called in the state transition from PREEOP to INIT
             to stop the mailbox handler. This functions informs the application
             about the state transition, the application cannot refuse
             the state transition.

 *////////////////////////////////////////////////////////////////////////////////////////

UINT16 APPL_StopMailboxHandler(void) 
{




/*ECATCHANGE_START(V5.12) TEST2*/
    bPendingESMLock = FALSE;
/*ECATCHANGE_END(V5.12) TEST2*/

/*ECATCHANGE_START(V5.12) TEST1*/
    bLocalErrorFlag = FALSE;
    u16LocalErrorCode = 0x00;
/*ECATCHANGE_END(V5.12) TEST1*/

    return ALSTATUSCODE_NOERROR;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param    pIntMask    pointer to the AL Event Mask which will be written to the AL event Mask
                        register (0x204) when this function is succeeded. The event mask can be adapted
                        in this function
 \return    AL Status Code (see ecatslv.h ALSTATUSCODE_....)

 \brief    The function is called in the state transition from PREOP to SAFEOP when
             all general settings were checked to start the input handler. This function
             informs the application about the state transition, the application can refuse
             the state transition when returning an AL Status error code.
            The return code NOERROR_INWORK can be used, if the application cannot confirm
            the state transition immediately, in that case the application need to be complete 
            the transition by calling ECAT_StateChange.
 *////////////////////////////////////////////////////////////////////////////////////////

UINT16 APPL_StartInputHandler(UINT16 *pIntMask) 
{

    /*ECATCHANGE_START(V5.12) TEST2*/
    if (IS_TEST_ACTIVE(ESM0_PENDING_TRANSITIONS))
    {
        if (bPendingESMLock)
        {
            //function is called the second time
            return ALSTATUSCODE_NOVALIDFIRMWARE;
        }
        else
        {
            INC_TEST_CNT(ESM0_PENDING_TRANSITIONS);
            bPendingESMLock = TRUE;
            return NOERROR_INWORK;
        }
    }
    /*ECATCHANGE_END(V5.12) TEST2*/

    return ALSTATUSCODE_NOERROR;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \return     0, NOERROR_INWORK

 \brief    The function is called in the state transition from SAFEOP to PREEOP
             to stop the input handler. This functions informs the application
             about the state transition, the application cannot refuse
             the state transition.

 *////////////////////////////////////////////////////////////////////////////////////////

UINT16 APPL_StopInputHandler(void) 
{

    /*ECATCHANGE_START(V5.12) TEST2*/
    bPendingESMLock = FALSE;
    /*ECATCHANGE_END(V5.12) TEST2*/

    return ALSTATUSCODE_NOERROR;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \return    AL Status Code (see ecatslv.h ALSTATUSCODE_....)

 \brief    The function is called in the state transition from SAFEOP to OP when
             all general settings were checked to start the output handler. This function
             informs the application about the state transition, the application can refuse
             the state transition when returning an AL Status error code.
           The return code NOERROR_INWORK can be used, if the application cannot confirm
           the state transition immediately, in that case the application need to be complete 
           the transition by calling ECAT_StateChange.
 *////////////////////////////////////////////////////////////////////////////////////////

UINT16 APPL_StartOutputHandler(void) 
{

    /*ECATCHANGE_START(V5.12) TEST2*/
    if (IS_TEST_ACTIVE(ESM0_PENDING_TRANSITIONS))
    {
        if (bPendingESMLock)
        {
            //function is called the second time
            return ALSTATUSCODE_NOVALIDFIRMWARE;
        }
        else
        {
            INC_TEST_CNT(ESM0_PENDING_TRANSITIONS);
            bPendingESMLock = TRUE;
            return NOERROR_INWORK;
        }
    }
    /*ECATCHANGE_END(V5.12) TEST2*/

    return ALSTATUSCODE_NOERROR;

}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \return     0, NOERROR_INWORK

 \brief    The function is called in the state transition from OP to SAFEOP
             to stop the output handler. This functions informs the application
             about the state transition, the application cannot refuse
             the state transition.

 *////////////////////////////////////////////////////////////////////////////////////////

UINT16 APPL_StopOutputHandler(void)
{

    /*ECATCHANGE_START(V5.12) TEST2*/
    bPendingESMLock = FALSE;
    /*ECATCHANGE_END(V5.12) TEST2*/


    return ALSTATUSCODE_NOERROR;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
\return     0(ALSTATUSCODE_NOERROR), NOERROR_INWORK
\param      pInputSize  pointer to save the input process data length
\param      pOutputSize  pointer to save the output process data length

\brief    This function calculates the process data sizes from the actual SM-PDO-Assign
            and PDO mapping
 *////////////////////////////////////////////////////////////////////////////////////////

UINT16 APPL_GenerateMapping(UINT16* pInputSize, UINT16* pOutputSize)
{

    UINT16 result = ALSTATUSCODE_NOERROR;
    UINT16 PDOAssignEntryCnt = 0;
    OBJCONST TOBJECT OBJMEM * pPDO = NULL;
    UINT16 PDOSubindex0 = 0;
    UINT32 *pPDOEntry = NULL;
    UINT16 PDOEntryCnt = 0;
    UINT16 InputSize = 0;
    UINT16 OutputSize = 0;

    /*Scan object 0x1C12 RXPDO assign*/
    for (PDOAssignEntryCnt = 0; PDOAssignEntryCnt < sRxPDOassign.u16SubIndex0; PDOAssignEntryCnt++) 
    {
        pPDO = OBJ_GetObjectHandle(sRxPDOassign.aEntries[PDOAssignEntryCnt]);
        if (pPDO != NULL) 
        {
            PDOSubindex0 = *((UINT16 *) pPDO->pVarPtr);
            for (PDOEntryCnt = 0; PDOEntryCnt < PDOSubindex0; PDOEntryCnt++) 
            {
/*ECATCHANGE_START(V5.12) ECAT1*/
                pPDOEntry = (UINT32 *)((UINT16 *)pPDO->pVarPtr + (OBJ_GetEntryOffset((UINT8)(PDOEntryCnt + 1), pPDO) >> 3) / 2); //goto PDO entry
/*ECATCHANGE_END(V5.12) ECAT1*/
                // we increment the expected output size depending on the mapped Entry
                OutputSize += (UINT16) ((*pPDOEntry) & 0xFF);
            }
        } 
        else
        {
            /*assigned PDO was not found in object dictionary. return invalid mapping*/
            OutputSize = 0;
            result = ALSTATUSCODE_INVALIDOUTPUTMAPPING;
            break;
        }
    }

    OutputSize = (OutputSize + 7) >> 3;

    if (result == 0) 
    {
        /*Scan Object 0x1C13 TXPDO assign*/
        for (PDOAssignEntryCnt = 0; PDOAssignEntryCnt < sTxPDOassign.u16SubIndex0; PDOAssignEntryCnt++) 
        {
            pPDO = OBJ_GetObjectHandle(sTxPDOassign.aEntries[PDOAssignEntryCnt]);
            if (pPDO != NULL) 
            {
                PDOSubindex0 = *((UINT16 *) pPDO->pVarPtr);
                for (PDOEntryCnt = 0; PDOEntryCnt < PDOSubindex0; PDOEntryCnt++) 
                {
/*ECATCHANGE_START(V5.12) ECAT1*/
                    pPDOEntry = (UINT32 *) ((UINT16 *) pPDO->pVarPtr + (OBJ_GetEntryOffset((UINT8)(PDOEntryCnt + 1), pPDO) >> 3)/2); //goto PDO entry
/*ECATCHANGE_END(V5.12) ECAT1*/
                    // we increment the expected output size depending on the mapped Entry
                    InputSize += (UINT16) ((*pPDOEntry) & 0xFF);
                }
            } 
            else
            {
                /*assigned PDO was not found in object dictionary. return invalid mapping*/
                InputSize = 0;
                result = ALSTATUSCODE_INVALIDINPUTMAPPING;
                break;
            }
        }
    }
    InputSize = (InputSize + 7) >> 3;

    *pInputSize = InputSize;
    *pOutputSize = OutputSize;
    return result;
}


/////////////////////////////////////////////////////////////////////////////////////////
/**
\param      pData  pointer to input process data
\brief      This function will copies the inputs from the local memory to the ESC memory
            to the hardware
 *////////////////////////////////////////////////////////////////////////////////////////

void APPL_InputMapping(UINT16* pData) 
{

    UINT16 *pTmpData = pData;
    UINT16 cnt = 0;

    for (cnt = 0; cnt < sTxPDOassign.u16SubIndex0; cnt++) 
    {
        switch (sTxPDOassign.aEntries[cnt]) 
        {
            case 0x1A02:
            {
                /*0x6020*/
                MEMCPY(pTmpData, &CounterOverrun, SIZEOF(CounterOverrun));
                if (cnt < (sTxPDOassign.u16SubIndex0 - 1))
                    pTmpData += 1;
            }
                break;
            case 0x1A00:
            {
                /*0x6000*/
                MEMCPY(pTmpData, &pdio_input_status, SIZEOF(UINT8));
                //MEMCPY(pTmpData, &InputCounter, SIZEOF(InputCounter));
                if (cnt < (sTxPDOassign.u16SubIndex0 - 1))
                    pTmpData += 2;
            }
                break;

            case 0x1AFF:
            {
                *pTmpData = SWAPWORD(nEcatStateTrans); /*0xA000.2, UINT16*/
                pTmpData++;

                *pTmpData = SWAPWORD(EsmTimeoutCounter); /*0xA000.2, Int16*/
                pTmpData++;

                *pTmpData = SWAPWORD(i16WaitForPllRunningTimeout); /*0xA000.3*/
                pTmpData++;

                *pTmpData = SWAPWORD(i16WaitForPllRunningCnt); /*0xA000.4*/
                pTmpData++;

                *pTmpData = SWAPWORD(u16LocalErrorCode); /*0xA000.5*/
                pTmpData++;

                *pTmpData = 0;
                *pTmpData |= bApplEsmPending; /*0xA000.6*/
                *pTmpData |= (bEcatWaitForAlControlRes << 1); /*0xA000.7*/
                *pTmpData |= (bEcatOutputUpdateRunning << 2); /*0xA000.8*/
                *pTmpData |= (bEcatInputUpdateRunning << 3); /*0xA000.9*/
                *pTmpData |= (bEcatFirstOutputsReceived << 4); /*0xA000.10*/
                *pTmpData |= (bLocalErrorFlag << 5); /*0xA000.11*/
                /*0xA000.12 2Bit Alignment*/

                *pTmpData |= (bWdTrigger << 8); /*0xA000.17*/
                *pTmpData |= (bDcSyncActive << 9); /*0xA000.18*/
                *pTmpData |= (bEscIntEnabled << 10); /*0xA000.19*/
                *pTmpData |= (bDcRunning << 11); /*0xA000.20*/
                *pTmpData |= (bSmSyncSequenceValid << 13); /*0xA000.21*/
                /*0xA000.22 3Bit Alignment*/
                pTmpData++;

                *pTmpData = SWAPWORD(EcatWdValue); /*0xA000.33, UINT16*/
                pTmpData++;
                *pTmpData = 0; /*EcatWdCounter; 0xA000.34 not available*/
                pTmpData++;

                *pTmpData = SWAPWORD(Sync0WdCounter); /*0xA000.35, UINT16*/
                pTmpData++;

                *pTmpData = SWAPWORD(Sync0WdValue); /*0xA000.36, UINT16*/
                pTmpData++;

                *pTmpData = SWAPWORD(Sync1WdCounter); /*0xA000.37, UINT16*/
                pTmpData++;

                *pTmpData = SWAPWORD(Sync1WdValue); /*0xA000.38 UINT16*/
                pTmpData++;

                *pTmpData = SWAPWORD(LatchInputSync0Value); /*0xA000.39, UINT16*/
                pTmpData++;

                *pTmpData = SWAPWORD(sSyncManOutPar.u16SmEventMissedCounter); /*0x1C32.11*/
                pTmpData++;
            }
                break;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
\param      pData  pointer to output process data

\brief    This function will copies the outputs from the ESC memory to the local memory
            to the hardware
 *////////////////////////////////////////////////////////////////////////////////////////

void APPL_OutputMapping(UINT16* pData) 
{
    UINT16 *pTmpData = pData;
    UINT16 cnt = 0;

    for (cnt = 0; cnt < sRxPDOassign.u16SubIndex0; cnt++) 
    {
        switch (sRxPDOassign.aEntries[cnt]) 
        {
            case 0x1602: //3: Baskar
            {
                /*Copy data of 0x7030*/
                MEMCPY(&OutputCounter1, pTmpData, SIZEOF(OutputCounter1));
                if (cnt < (sRxPDOassign.u16SubIndex0 - 1))
                    pTmpData += 2;
            }
                break;
            case 0x1601:
            {
                /*copy data of 0x7010*/
               // MEMCPY(&OutputCounter, pTmpData, SIZEOF(OutputCounter));
                MEMCPY(&OutputCounter, pTmpData, SIZEOF(OutputCounter));
                if (cnt < (sRxPDOassign.u16SubIndex0 - 1))
                    pTmpData += 2;
            }
                break;
        }
    }

}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \brief     Application which is called with every cycle (the cycle depends on the mode of synchronisation)
 *////////////////////////////////////////////////////////////////////////////////////////

void APPL_Application(void) 
{

    UINT32 ActiveMask = 0;
    UINT16 cnt = 0;


    /*update test control*/
    HW_EscReadDWord(ActiveMask, 0xF10);

        {
            for (cnt = 0; cnt < sTestControlObj.u16SubIndex0; cnt++) 
            {
                ActiveMask = (ActiveMask >> cnt);

                if (sTestControlObj.aEntries[cnt].pTest != NULL) 
                {
                    if (ActiveMask & 0x1)
                        sTestControlObj.aEntries[cnt].pTest->Control |= 0x1;
                    else
                        sTestControlObj.aEntries[cnt].pTest->Control &= ~0x1;
                }
            }
        }
    if (OutputCounter > 0) 
    {
        InputCounter = OutputCounter + 1;
        CounterOverrun = 0;
    }
    else 
    {
        InputCounter=0x55;//++; //baskar
       // if (InputCounter == 0xFFFFFFFF)
          //  CounterOverrun++;
    }


    if(sObj300D_S2S.DstSlv != 0)
    {
        if((sObj300D_S2S.Command != sObj300D_S2S.Status) && (sObj300D_S2S.Command == 0x1))
        {
            TINITSDOUPLOADREQMBX MBXMEM *pMbx = (TINITSDOUPLOADREQMBX MBXMEM *) APPL_AllocMailboxBuffer(16); // allocate buffer for the 
            HMEMSET(pMbx,0x00,16); //clear allocated buffer 

            if (pMbx)
            {
                pMbx->MbxHeader.Address = sObj300D_S2S.DstSlv;
                pMbx->MbxHeader.Flags[MBX_OFFS_TYPE] |= (MBX_TYPE_COE << MBX_SHIFT_TYPE);

                pMbx->CoeHeader |= ((UINT16)COESERVICE_SDOREQUEST) << COEHEADER_COESERVICESHIFT;
                pMbx->SdoHeader.Sdo[SDOHEADER_COMMANDOFFSET] |= SDOSERVICE_INITIATEUPLOADREQ;

                pMbx->MbxHeader.Length = EXPEDITED_FRAME_SIZE;
                pMbx->SdoHeader.Sdo[SDOHEADER_INDEXHIOFFSET] = 0x10;
                pMbx->SdoHeader.Sdo[SDOHEADER_INDEXLOOFFSET] |= (0x18 << SDOHEADER_INDEXLOSHIFT);
                pMbx->SdoHeader.Sdo[SDOHEADER_SUBINDEXOFFSET] |= (0x2 << SDOHEADER_SUBINDEXSHIFT);


                if (MBX_MailboxSendReq((TMBX MBXMEM *)pMbx, COE_SERVICE) != 0)
                {
                    APPL_FreeMailboxBuffer(pMbx);
                }

            } //Mbox buffer was allocated

            sObj300D_S2S.Command = 0;
            sObj300D_S2S.Status = 1; //set status to wait for response

        } // an command is pending
    } //an slave destination address is set

/*ECATCHANGE_START(V5.12) TEST1*/
    /* try to overwrite the current Error */
    if ((nAlStatus == 0x14) && (!bLocalErrorFlag))
    {
        ECAT_StateChange((STATE_CHANGE | STATE_SAFEOP), ALSTATUSCODE_NOVALIDFIRMWARE);
    }


/*ECATCHANGE_END(V5.12) TEST1*/



}


/////////////////////////////////////////////////////////////////////////////////////////
/**
 \brief    Update test active states (called on startup and every ALControl Indication (INIT_2_ANY)
 *////////////////////////////////////////////////////////////////////////////////////////

void UpdateActiveTest() 
{
    HandleTestControlViaUserRAM();
}
/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param    index              index of the requested object.
 \param    subindex           subindex of the requested object.
 \param    dataSize           received data size of the SDO Download
 \param    pData              Pointer to the buffer where the written data can be copied from
 \param    bCompleteAccess    Indicates if a complete write of all subindices of the
                              object shall be done or not

 \return    result of the write operation (0 (success) or an abort code (ABORTIDX_.... defined in
            sdosrv.h))

 \brief     This function writes the test control object
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 WriteObject0x2FFF(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    OBJCONST TOBJECT OBJMEM * pObject = NULL;
    TTESTOBJ OBJMEM * pTestObject = NULL;
    UINT32 NewTest = 0;
    UINT16 NewTestIndex = 0;
    UINT8 NewTestSubIndex = 0;

    /*Complete Access is not supported for Test objects*/
    if (bCompleteAccess)
        return ABORTIDX_UNSUPPORTED_ACCESS;

    /*Subindex 0 is read only*/
    if (subindex == 0)
        return ABORTIDX_READ_ONLY_ENTRY;

    /*handle only test control object with this function*/
    if (index != 0x2FFF)
        return ABORTIDX_OBJECT_NOT_EXISTING;

    /*Check Data (it is only valid to write low Byte, the high Byte contains status information)*/
    MBXMEMCPY(&NewTest, pData, 4);
    NewTestIndex = (UINT16) (NewTest >> 16);
    NewTestSubIndex = (UINT8) ((NewTest >> 8) & 0xFF);

    pObject = OBJ_GetObjectHandle(NewTestIndex);

    /*new index is not available*/
    if (NewTestIndex != 0 && (pObject == NULL || NewTestIndex < 0x2000 || NewTestIndex > 0x2FFD))
        return ABORTIDX_VALUE_EXCEEDED;

    /*disable current controlled test*/
    if (sTestControlObj.aEntries[(subindex-1)].pTest != NULL) 
    {
        sTestControlObj.aEntries[(subindex - 1)].pTest->Control &= ~0x1;
    }

    if (NewTestIndex == 0) 
    {
        /*reset entry*/
        sTestControlObj.aEntries[(subindex - 1)].pTest = NULL;
        sTestControlObj.aEntries[(subindex - 1)].TestIndex = 0;
        sTestControlObj.aEntries[(subindex - 1)].TestSubindex = 0;
    }
    else 
    {
        /*update test control entry*/
        pTestObject = (TTESTOBJ OBJMEM *) pObject->pVarPtr;

        if (pTestObject != NULL) 
        {
            sTestControlObj.aEntries[(subindex - 1)].pTest = pTestObject->paEntries[NewTestSubIndex]; //Get handle to test
        }
        sTestControlObj.aEntries[(subindex - 1)].TestIndex = NewTestIndex;
        sTestControlObj.aEntries[(subindex - 1)].TestSubindex = NewTestSubIndex;
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param     index               index of the requested object.
 \param     subindex            subindex of the requested object.
 \param     objSize             size of the requested object data, calculated with OBJ_GetObjectLength
 \param     pData               Pointer to the buffer where the data can be copied to
 \param     bCompleteAccess     Indicates if a complete read of all subindices of the
                                object shall be done or not

 \return    result of the read operation (0 (success) or an abort code (ABORTIDX_.... defined in
            sdosrv.h))

 \brief     This function reads the test control object
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 ReadObject0x2FFF(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    UINT16 *pTmpData = pData;

    /*Complete Access is not supported for Test objects*/
    if (bCompleteAccess)
        return ABORTIDX_UNSUPPORTED_ACCESS;

    /*handle only test control object with this function*/
    if (index != 0x2FFF)
        return ABORTIDX_OBJECT_NOT_EXISTING;

    if (subindex > TEST_CONTROL_BITS)
        return ABORTIDX_SUBINDEX_NOT_EXISTING;

    if (subindex > 0) 
    {
        *pTmpData = ((UINT16) sTestControlObj.aEntries[(subindex - 1)].TestSubindex) << 8;
        pTmpData++;
        *pTmpData = sTestControlObj.aEntries[(subindex - 1)].TestIndex;
    }
    else
    {
        *pData = sTestControlObj.u16SubIndex0;
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param    index              index of the requested object.
 \param    subindex           subindex of the requested object.
 \param    dataSize           received data size of the SDO Download
 \param    pData              Pointer to the buffer where the written data can be copied from
 \param    bCompleteAccess    Indicates if a complete write of all subindices of the
                              object shall be done or not

 \return    result of the write operation (0 (success) or an abort code (ABORTIDX_.... defined in
            sdosrv.h))

 \brief     This function writes the Bit3 Array object
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 WriteObject0x3007(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    UINT8 result = 0;

    if (subindex > sObj3007.subindex0) 
    {
        return ABORTIDX_SUBINDEX_NOT_EXISTING;
    }


    if (subindex == 0) 
    {
        sObj3007.subindex0 = SWAPWORD(*pData);

        if (dataSize > 2)
            pData++;
    }


    if (bCompleteAccess) 
    {
        if (subindex > 1)
            return ABORTIDX_UNSUPPORTED_ACCESS;

        sObj3007.Value = SWAPWORD(*pData);
    }
    else
    {
        if (subindex > 0) 
        {
            UINT16 TmpVal = SWAPWORD(*pData) << ((subindex - 1) * 3);

            UINT16 Mask = 0x3 << ((subindex - 1) * 3); //get BitMask for corresponding subindex

            sObj3007.Value &= ~Mask;
            sObj3007.Value |= TmpVal;
        }
    }

    return result;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param     index               index of the requested object.
 \param     subindex            subindex of the requested object.
 \param     objSize             size of the requested object data, calculated with OBJ_GetObjectLength
 \param     pData               Pointer to the buffer where the data can be copied to
 \param     bCompleteAccess     Indicates if a complete read of all subindices of the
                                object shall be done or not

 \return    result of the read operation (0 (success) or an abort code (ABORTIDX_.... defined in
            sdosrv.h))

 \brief     This function reads the Bit3 Array object
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 ReadObject0x3007(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    UINT8 result = 0;

    if (subindex > sObj3007.subindex0) 
    {
        return ABORTIDX_SUBINDEX_NOT_EXISTING;
    }

    HMEMSET(pData, 0x00, dataSize);

    if (subindex == 0) 
    {
        *pData = SWAPWORD(sObj3007.subindex0);

        if (dataSize > 2)
            pData++;
    }


    if (bCompleteAccess) 
    {
        if (subindex > 1)
            return ABORTIDX_UNSUPPORTED_ACCESS;

        *pData = SWAPWORD(sObj3007.Value);
    }
    else
    {
        if (subindex > 0) 
        {
            UINT16 TmpVal = SWAPWORD(sObj3007.Value) >> ((subindex - 1) * 3);

            *pData = (TmpVal & 0x3);
        }
    }


    return result;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param     index               index of the requested object.
 \param     subindex            subindex of the requested object.
 \param     objSize             size of the requested object data, calculated with OBJ_GetObjectLength
 \param     pData               Pointer to the buffer where the data can be copied to
 \param     bCompleteAccess     Indicates if a complete read of all subindices of the
                                object shall be done or not

 \return    result of the read operation (0 (success) or an abort code (ABORTIDX_.... defined in
            sdosrv.h))

 \brief     This function reads the slave configuration object
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 ReadObject0x8000(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    /*Complete Access is not supported for Test objects*/
    if (bCompleteAccess)
        return ABORTIDX_UNSUPPORTED_ACCESS;

    if (subindex > 0) 
    {
        *pData = (UINT8) aSlaveConfig[(subindex - 1)].bValue;
    }
    else
    {
        *pData = (SIZEOF(aSlaveConfig) / SIZEOF(TSLV_CONFIG));
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param     index               index of the requested object.
 \param     subindex            subindex of the requested object.
 \param     objSize             size of the requested object data, calculated with OBJ_GetObjectLength
 \param     pData               Pointer to the buffer where the data can be copied to
 \param     bCompleteAccess     Indicates if a complete read of all subindices of the
                                object shall be done or not

 \return    result of the read operation (0 (success) or an abort code (ABORTIDX_.... defined in
            sdosrv.h))

 \brief     This function reads the ESM variables object 0xA000
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 ReadObject0xA000(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) {
    /*Complete Access is not supported for ESM variables object*/
    if (bCompleteAccess)
        return ABORTIDX_UNSUPPORTED_ACCESS;

    if (index != 0xA000)
        return ABORTIDX_UNSUPPORTED_ACCESS;

    if (pData == NULL)
        return ABORTIDX_OUT_OF_MEMORY;


    *pData = 0;

    switch (subindex) {
        case 0:
            *pData = A000Subindex0;
            break;
        case 1: /* nEcatStateTrans (UINT16)*/
            *pData = nEcatStateTrans;
            break;
        case 2: /* EsmTimeoutCounter (Int16)*/
            *pData = EsmTimeoutCounter;
            break;
        case 3: /* i16WaitForPllRunningTimeout*/
            *pData = i16WaitForPllRunningTimeout;
            break;
        case 4: /* i16WaitForPllRunningCnt*/
            *pData = i16WaitForPllRunningCnt;
            break;
        case 5: /* u16LocalErrorCode*/
            *pData = u16LocalErrorCode;
            break;
        case 6: /* bApplEsmPending*/
            *pData = bApplEsmPending;
            break;
        case 7: /* bEcatWaitForAlControlRes*/
            *pData = bEcatWaitForAlControlRes;
            break;
        case 8: /* bEcatOutputUpdateRunning*/
            *pData = bEcatOutputUpdateRunning;
            break;
        case 9: /* bEcatInputUpdateRunning*/
            *pData = bEcatInputUpdateRunning;
            break;
        case 10: /* bEcatFirstOutputsReceived*/
            *pData = bEcatFirstOutputsReceived;
            break;
        case 11: /* bLocalErrorFlag*/
            *pData = bLocalErrorFlag;
            break;
        case 17: /* bWdTrigger*/
            *pData = bWdTrigger;
            break;
        case 18: /* bDcSyncActive*/
            *pData = bDcSyncActive;
            break;
        case 19: /* bEscIntEnabled*/
            *pData = bEscIntEnabled;
            break;
        case 20: /* bDcRunning*/
            *pData = bDcRunning;
            break;
        case 21: /* bSmSyncSequenceValid*/
            *pData = bSmSyncSequenceValid;
            break;
        case 33: /* EcatWdValue (UINT16)*/
            *pData = EcatWdValue;
            break;
        case 34: /* EcatWdCounter (UINT16)*/
            *pData = 0;
            break;
        case 35: /* Sync0WdCounter (UINT16)*/
            *pData = Sync0WdCounter;
            break;
        case 36: /* Sync0WdValue (UINT16)*/
            *pData = Sync0WdValue;
            break;
        case 37: /* Sync1WdCounter (UINT16)*/
            *pData = Sync1WdCounter;
            break;
        case 38: /* Sync1WdValue (UINT16)*/
            *pData = Sync1WdValue;
            break;
        case 39: /* LatchInputSync0Value (UINT16)*/
            *pData = LatchInputSync0Value;
            break;
        default:
            return ABORTIDX_SUBINDEX_NOT_EXISTING;
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param    index              index of the requested object.
 \param    subindex           subindex of the requested object.
 \param    dataSize           received data size of the SDO Download
 \param    pData              Pointer to the buffer where the written data can be copied from
 \param    bCompleteAccess    Indicates if a complete write of all subindices of the
                              object shall be done or not

 \return    result of the write operation (0 (success) or an abort code (ABORTIDX_.... defined in
            sdosrv.h))

 \brief     This function writes the test objects
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 WriteTestObject(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    OBJCONST TOBJECT OBJMEM * pObject = NULL;
    TTESTOBJ OBJMEM * pTestObject = NULL;
    UINT16 Value = 0x0000;

    /*Complete Access is not supported for Test objects*/
    if (bCompleteAccess)
        return ABORTIDX_UNSUPPORTED_ACCESS;

    /*Subindex 0 is read only*/
    if (subindex == 0)
        return ABORTIDX_READ_ONLY_ENTRY;

    /*check if the data size is valid*/
    if (dataSize != BIT2BYTE(TEST_ENTRY_BIT_LENGTH))
        return ABORTIDX_PARAM_LENGTH_ERROR;


    pObject = OBJ_GetObjectHandle(index);

    if (pObject == NULL)
        return ABORTIDX_OBJECT_NOT_EXISTING;

    pTestObject = (TTESTOBJ OBJMEM *) pObject->pVarPtr;

    if (pTestObject == NULL)
        return ABORTIDX_GENERAL_ERROR;

    /*Check Data (it is only valid to write low Byte, the high Byte contains status information)*/
    Value = SWAPWORD(*pData);
    if (Value & 0xFF00)
        return ABORTIDX_VALUE_EXCEEDED;

    pTestObject->paEntries[(subindex - 1)]->Control = Value;
    pTestObject->paEntries[(subindex - 1)]->Counter = 0;

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param     index               index of the requested object.
 \param     subindex            subindex of the requested object.
 \param     objSize             size of the requested object data, calculated with OBJ_GetObjectLength
 \param     pData               Pointer to the buffer where the data can be copied to
 \param     bCompleteAccess     Indicates if a complete read of all subindices of the
                                object shall be done or not

 \return    result of the read operation (0 (success) or an abort code (ABORTIDX_.... defined in
            sdosrv.h))

 \brief     This function reads the test objects
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 ReadTestObject(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    OBJCONST TOBJECT OBJMEM * pObject = NULL;
    TTESTOBJ OBJMEM * pTestObject = NULL;
    UINT16 Value = 0x0000;

    /*Complete Access is not supported for Test objects*/
    if (bCompleteAccess)
        return ABORTIDX_UNSUPPORTED_ACCESS;

    pObject = OBJ_GetObjectHandle(index);

    if (pObject == NULL)
        return ABORTIDX_OBJECT_NOT_EXISTING;

    pTestObject = (TTESTOBJ OBJMEM *) pObject->pVarPtr;

    if (pTestObject == NULL)
        return ABORTIDX_GENERAL_ERROR;

    if (subindex == 0) 
    {
        Value = pTestObject->u16SubIndex0;

        *pData = (UINT8) (Value & 0xFF);
    }
    else
   {
        Value = (pTestObject->paEntries[(subindex - 1)]->Counter << 8) | pTestObject->paEntries[(subindex - 1)]->Control;

        *pData = SWAPWORD(Value);
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param    index              index of the requested object.
 \param    subindex           subindex of the requested object.
 \param    dataSize           received data size of the SDO Download
 \param    pData              Pointer to the buffer where the written data can be copied from
 \param    bCompleteAccess    Indicates if a complete write of all subindices of the
                              object shall be done or not

 \return    ABORTIDX_WORKING

 \brief     This function stores the values to handle the SDO request afterwards
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 WriteObject0x3006(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    if (SDOResult == ABORTIDX_WORKING) 
    {
        if (!IS_TEST_ACTIVE(DelaySdoResponse0x3006) || (bUnlockSdoRequest == TRUE))
        {
            if (HUGE_OBJ_BYTE_SIZE < dataSize)
                return ABORTIDX_PARAM_LENGTH_TOO_LONG;

            MEMCPY(HugeObj, pData, dataSize);

            SDOResult = 0;
            bUnlockSdoRequest = FALSE;
        }
        else
        {
            /*SDO request is still pending*/
            SDOResult = ABORTIDX_WORKING;
        }
    }
    else
    {
        /*Set SDO access to pending*/
        SDOResult = ABORTIDX_WORKING;
    }

    if (SDOResult == ABORTIDX_WORKING)
    {
        bUnlockSdoRequest = FALSE;
    }

    return SDOResult;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param     index               index of the requested object.
 \param     subindex            subindex of the requested object.
 \param     objSize             size of the requested object data, calculated with OBJ_GetObjectLength
 \param     pData               Pointer to the buffer where the data can be copied to
 \param     bCompleteAccess     Indicates if a complete read of all subindices of the
                                object shall be done or not

 \return    ABORTIDX_WORKING

 \brief     This function stores the values to handle the SDO request afterwards
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 ReadObject0x3006(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    if (SDOResult == ABORTIDX_WORKING) 
    {
        if (!IS_TEST_ACTIVE(DelaySdoResponse0x3006) || (bUnlockSdoRequest == TRUE))
        {

            if (HUGE_OBJ_BYTE_SIZE < dataSize)
                return ABORTIDX_PARAM_LENGTH_TOO_LONG;

            MEMCPY(pData, HugeObj, dataSize);

            SDOResult = 0;
            bUnlockSdoRequest = FALSE;
        }
        else
        {
            /*SDO request is still pending*/
            SDOResult = ABORTIDX_WORKING;
        }
    }
    else
    {
        /*Set SDO access to pending*/
        SDOResult = ABORTIDX_WORKING;
    }

    if (SDOResult == ABORTIDX_WORKING)
    {
        bUnlockSdoRequest = FALSE;
    }
    return SDOResult;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param    index              index of the requested object.
 \param    subindex           subindex of the requested object.
 \param    dataSize           received data size of the SDO Download
 \param    pData              Pointer to the buffer where the written data can be copied from
 \param    bCompleteAccess    Indicates if a complete write of all subindices of the
                              object shall be done or not

 \return    ABORTIDX_WORKING

 \brief     This function stores the values to handle the SDO request afterwards
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 WriteObject0x3002(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    if (SDOResult == ABORTIDX_WORKING) 
    {
        u32Obj = 0;

        if (SIZEOF(u32Obj) < dataSize)
            return ABORTIDX_PARAM_LENGTH_TOO_LONG;

        MEMCPY(&u32Obj, pData, dataSize);

        SDOResult = 0;


    }
    else
    {
        /*Set SDO access to pending*/
        SDOResult = ABORTIDX_WORKING;
    }
    return SDOResult;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param     index               index of the requested object.
 \param     subindex            subindex of the requested object.
 \param     objSize             size of the requested object data, calculated with OBJ_GetObjectLength
 \param     pData               Pointer to the buffer where the data can be copied to
 \param     bCompleteAccess     Indicates if a complete read of all subindices of the
                                object shall be done or not

 \return    ABORTIDX_WORKING

 \brief     This function stores the values to handle the SDO request afterwards
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 ReadObject0x3002(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    if (SDOResult == ABORTIDX_WORKING) 
    {
        if (SIZEOF(u32Obj) < dataSize)
            return ABORTIDX_PARAM_LENGTH_TOO_LONG;

        MEMCPY(pData, &u32Obj, dataSize);

        SDOResult = 0;
    }
    else
    {
        /*Set SDO access to pending*/
        SDOResult = ABORTIDX_WORKING;
    }
    return SDOResult;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param    index              index of the requested object.
 \param    subindex           subindex of the requested object.
 \param    dataSize           received data size of the SDO Download
 \param    pData              Pointer to the buffer where the written data can be copied from
 \param    bCompleteAccess    Indicates if a complete write of all subindices of the
                              object shall be done or not

 \return    0 or SDO ABORT CODE

 \brief     This functions handles write access to the huge array object.
            The entry size can be modified by an single WORD access to SI1. All other write accesses will be ignored (no Abort code will be returned)
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 WriteObject0x3009_0x300A(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    if (index == 0x3009 || index == 0x300A) 
    {
        if (subindex == 1 && (bCompleteAccess == FALSE)) 
        {
            if (dataSize == 2)
                asEntryDesc0x3009[1].BitLength = *pData;
        }
    }
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param     index               index of the requested object.
 \param     subindex            subindex of the requested object.
 \param     dataSize             size of the requested object data, calculated with OBJ_GetObjectLength
 \param     pData               Pointer to the buffer where the data can be copied to
 \param     bCompleteAccess     Indicates if a complete read of all subindices of the
                                object shall be done or not

 \return    0 or SDO ABORT CODE

 \brief     This function stores the values to handle the SDO request afterwards
 *////////////////////////////////////////////////////////////////////////////////////////

UINT8 ReadObject0x3009_0x300A(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess) 
{
    if (subindex == 0) 
    {
        *pData = sObj3009.subindex0;

        if (dataSize >= 2) 
        {
            pData++;
            dataSize -= 2;
        }
        else
        {
            dataSize = 0;
        } 
    }

    if (dataSize >= 2 && (subindex == 1 || bCompleteAccess)) 
    {
        //copy current configured bit size at the beginning of the SI1
        *pData = asEntryDesc0x3009[1].BitLength;
        pData++;
        dataSize -= 2;
    }

    if (dataSize > 0) 
    {
        //fill all following bytes with defined value
        HMEMSET(pData, sObj3009.Value, dataSize);
    }

    return 0;
}

UINT8 WriteEoeSendData(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess)
{
    if (index != 0x3017)
        return ABORTIDX_UNSUPPORTED_ACCESS;
    
    if (subindex > 0)
        return ABORTIDX_SUBINDEX_NOT_EXISTING;

    if (bCompleteAccess)
        return ABORTIDX_UNSUPPORTED_ACCESS;
    
    //free memory if it was not freed by the stack before
    if (i8EoeSendPending != 0 && pObj3017_EoeSendData != NULL)
        FREEMEM(pObj3017_EoeSendData);

    if (dataSize > 0)
    {
        pObj3017_EoeSendData = ALLOCMEM(dataSize);
        if (pObj3017_EoeSendData == NULL)
            return ABORTIDX_OUT_OF_MEMORY;

        Obj3017_EoeSendDataLength = (UINT16)dataSize;

        HMEMCPY(pObj3017_EoeSendData, pData, dataSize);

        i8EoeSendPending = 2;
    }

    return 0;
}
UINT8 ReadEoeSendData(UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess)
{
    if (index != 0x3017)
        return ABORTIDX_UNSUPPORTED_ACCESS;

    if (subindex > 0)
        return ABORTIDX_SUBINDEX_NOT_EXISTING;

    if (bCompleteAccess)
        return ABORTIDX_UNSUPPORTED_ACCESS;

    if (pObj3017_EoeSendData == NULL)
    {
        HMEMSET(pData, dataSize, 0x00);
    }

    if (dataSize > Obj3017_EoeSendDataLength)
        return ABORTIDX_VALUE_EXCEEDED;

    HMEMCPY(pData,pObj3017_EoeSendData, dataSize);

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////
/**
 \param     index               index of the requested object.
 \param     subindex            subindex of the requested object.
 \param     dataSize             size of the requested object data, calculated with OBJ_GetObjectLength
 \param     pData               Pointer to the buffer where the data can be copied to
 \param     bCompleteAccess     Indicates if a complete read of all subindices of the
                                object shall be done or not

 \return    0 or SDO ABORT CODE

 \brief     This function handles read access to object 0x300C
 *////////////////////////////////////////////////////////////////////////////////////////
UINT8 ReadObject0x300C( UINT16 index, UINT8 subindex, UINT32 dataSize, UINT16 MBXMEM * pData, UINT8 bCompleteAccess )
{
    HMEMSET(pData,0xBA,dataSize);
    return 0;
}



/////////////////////////////////////////////////////////////////////////////////////////
/**

 \brief    This function shall be called cyclic during init state.
            It is checked if master has written a new test request to the ESC User RAM (0xF80:0xF83)

 *////////////////////////////////////////////////////////////////////////////////////////

void HandleTestControlViaUserRAM()
{
    UINT32 RegValue = 0;
    UINT16 index = 0;
    UINT8 SubIdx = 0;

    HW_EscReadDWord(RegValue, 0xF80);

    if (RegValue != CurrentActiveTest || (RegValue == 0))
    {
        index = (UINT16)((CurrentActiveTest & 0x0000FFFF));
        SubIdx = (UINT8)((CurrentActiveTest & 0x00FF0000) >> 16);

        if (IS_TEST_OBJECT(index))
        {
            /* Disable current Test*/
            OBJCONST TOBJECT OBJMEM *pObject = OBJ_GetObjectHandle(index);

            if (pObject != NULL)
            {
                TTESTOBJ OBJMEM * pTestObject = (TTESTOBJ OBJMEM *) pObject->pVarPtr;

                if (pTestObject != NULL)
                {
                    pTestObject->paEntries[(SubIdx - 1)]->Control = 0;
#ifdef DebugMessage
                    DebugMessage("Test: 0x%X.%X disabled (%d times triggered)\n",pObject->Index, SubIdx,pTestObject->paEntries[(SubIdx - 1)]->Counter);
#endif

                    pTestObject->paEntries[(SubIdx - 1)]->Counter = 0;
                }
            }

        }

        index = (UINT16)((RegValue & 0x0000FFFF));
        SubIdx = (UINT8)((RegValue & 0x00FF0000) >> 16);

        /* Enable New Test*/
        if (IS_TEST_OBJECT(index))
        {
            OBJCONST TOBJECT OBJMEM *pObject = NULL;

            pObject = OBJ_GetObjectHandle(index);

            if (pObject != NULL) {
                TTESTOBJ OBJMEM * pTestObject = (TTESTOBJ OBJMEM *) pObject->pVarPtr;

                if (pTestObject != NULL)
                {
                    pTestObject->paEntries[(SubIdx - 1)]->Control = TESTS_ENABLED;
#ifdef DebugMessage
                    DebugMessage("Test: 0x%X.%X enabled\n",pObject->Index, SubIdx);
#endif
                }
            }
        }
        else
        {
#ifdef DebugMessage
            DebugMessage("Default slave behaviour\n");
#endif
        }

        CurrentActiveTest = RegValue;
    }

    /* Update the test state in User RAM*/
    RegValue |= 0x01000000; /* set activate indication */

    HW_EscWriteDWord(RegValue, 0xF80);
    
}

/////////////////////////////////////////////////////////////////////////////////////////
/**

 \brief    This is the main function

 *////////////////////////////////////////////////////////////////////////////////////////

void ethercat_main(void)  //baskar
{
   UINT16 result = 0;
  
#ifdef PERF_TEST
    LONGLONG g_Frequency, g_CurentCount, g_LastCount, g_diff, g_sum;
    UINT32 counter = 0;

    g_sum = 0;
#endif    

    result = HW_Init();

    if (result != 0) 
    {
        HW_Release();

        return;
    }

    MainInit();

    APPL_Init();



#ifdef PERF_TEST
#ifdef DebugMessage
    if (QueryPerformanceFrequency((LARGE_INTEGER*) & g_Frequency))
        DebugMessage("frequency %d/s\n", g_Frequency);
#endif

    g_Frequency = g_Frequency / 1000000; //get ticks /us

    QueryPerformanceCounter((LARGE_INTEGER*) & g_CurentCount);
    QueryPerformanceCounter((LARGE_INTEGER*) & g_LastCount);
    g_diff = g_LastCount - g_CurentCount;

#ifdef DebugMessage
    DebugMessage("Querydiff %d\n", g_diff);
#endif
    g_CurentCount = 0;
#endif


    bRunApplication = TRUE;





    do {

        MainLoop();




#ifdef PERF_TEST
        QueryPerformanceCounter((LARGE_INTEGER*) & g_LastCount);
        if (g_CurentCount > 0) 
        {
            g_sum += g_LastCount - g_CurentCount - g_diff;
            counter++;
        }

        g_CurentCount = g_LastCount;



        if (counter > 100000) 
        {
            g_sum = ((g_sum / counter) / g_Frequency);
#ifdef DebugMessage
            DebugMessage("Delay %d\n", g_sum);
#endif
            counter = 0;
            g_sum = 0;
            g_CurentCount = 0;
        }
#endif

    } while (bRunApplication == TRUE);

    HW_Release();
}



/** @} */


