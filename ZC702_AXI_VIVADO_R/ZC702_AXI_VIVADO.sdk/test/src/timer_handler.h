#ifndef __TIMER_HANDLER_H_
#define __TIMER_HANDLER_H_

extern int timer_handler_init(void);
extern void start_timer(void);

extern volatile unsigned long TimerExpired;

#endif
