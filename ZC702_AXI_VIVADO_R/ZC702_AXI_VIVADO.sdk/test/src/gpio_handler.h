#ifndef __GPIOD_HANDLER_H_
#define __GPIOD_HANDLER_H_

#include <stdio.h>
#include "platform.h"

#include "xparameters.h"
#include "xgpio.h"
#include "xscugic.h"
#include "xil_exception.h"
#include "xil_printf.h"
#include "ecatappl.h"

#define INPUT_DEVICE_ID		XPAR_GPIO_0_DEVICE_ID  //XPAR_AXI_GPIO_0_DEVICE_ID
#define OUTPUT_DEVICE_ID	XPAR_GPIO_2_DEVICE_ID //XPAR_AXI_GPIO_1_DEVICE_ID

#define OUTPUT_CHANNEL		1
#define INPUT_CHANNEL		2

extern int GPIO_Init(void);
extern void GPIO_Input_Read(void);
extern unsigned int pdio_input_status;
extern XGpio Output_Drive, Input_Status;
#endif
