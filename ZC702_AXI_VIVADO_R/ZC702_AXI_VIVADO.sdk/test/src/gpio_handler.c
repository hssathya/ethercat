/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include "gpio_handler.h"
// Parameter definitions




XGpio Output_Drive, Input_Status;


unsigned int pdio_input_status=0;
//----------------------------------------------------
// GPIO Input Status Read FUNCTIONS
//----------------------------------------------------

void GPIO_Input_Read()
{
	static unsigned char Toggle_Flag=0;
	static unsigned int input_prev;
	unsigned int input_current;

	if(Toggle_Flag)
	{

		input_current = XGpio_DiscreteRead(&Input_Status, INPUT_CHANNEL);
		if(input_current==input_prev)
		{
			pdio_input_status=input_current;
			//XGpio_DiscreteWrite(&Output_Drive, OUTPUT_CHANNEL, pdio_input_status);
		}
		input_current=0;
		input_prev=0;
		Toggle_Flag=0;
	}
	else
	{
		Toggle_Flag=1;
		input_prev = XGpio_DiscreteRead(&Input_Status, INPUT_CHANNEL);
	}



}




int GPIO_Init() {
//	init_platform();

	int status;
	//----------------------------------------------------
	// INITIALIZE THE PERIPHERALS & SET DIRECTIONS OF GPIO
	//----------------------------------------------------
	// Initialise LEDs
	status = XGpio_Initialize(&Output_Drive, OUTPUT_DEVICE_ID);
	if (status != XST_SUCCESS)
		return XST_FAILURE;

	// Set LEDs direction to outputs
	XGpio_SetDataDirection(&Output_Drive, OUTPUT_CHANNEL, 0x00000);

	// Initialise Push Buttons
	status = XGpio_Initialize(&Input_Status, INPUT_DEVICE_ID);
	if (status != XST_SUCCESS)
		return XST_FAILURE;

	// Set all buttons direction to inputs
	XGpio_SetDataDirection(&Input_Status, INPUT_CHANNEL, 0xFFFFF);

	XGpio_DiscreteWrite(&Output_Drive, OUTPUT_CHANNEL, 0xFFFFF);
	return 0;
}
